package com.example.riskaadhita.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.riskaadhita.R
import com.example.riskaadhita.adapter.RepoAdapter
import com.example.riskaadhita.api.DataResponse
import com.example.riskaadhita.base.BaseActivity
import com.example.riskaadhita.data.Item
import com.example.riskaadhita.data.ItemRepo
import com.example.riskaadhita.helper.RecyclerViewClickListener
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail.rv_list
import kotlinx.android.synthetic.main.activity_detail.tv_loading
import kotlinx.android.synthetic.main.activity_detail.tv_note
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_users.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DetailActivity : BaseActivity() {
    override fun getView(): Int = R.layout.activity_detail
    var user = Item()
    lateinit var adapter: RepoAdapter
    var listRepo: ArrayList<ItemRepo> = arrayListOf()
    val nullValue: String = "-";

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        user = gson.fromJson(intent.getStringExtra("data"), Item::class.java)
        setRecycleView()
        getData()
    }

    fun setRecycleView(){
        adapter = RepoAdapter(this)
        rv_list.adapter = adapter
        initiateLinearLayoutRecyclerView(rv_list, object : RecyclerViewClickListener {
            override fun onClick(view: View, position: Int) {
                Toast.makeText(this@DetailActivity, adapter.lists[position].name, Toast.LENGTH_SHORT).show()
            }

            override fun onLongClick(view: View, position: Int) {
            }
        })
    }

    private fun getData(){
        startLoading()
        var url = user.url!!.split("api.github.com/")[1]
        val call: Call<Item> = baseApi.getDetailUser(url)
        call.enqueue(object : Callback<Item>{
            override fun onResponse(
                call: Call<Item>?,
                response: Response<Item>?
            ) {
                if(response!!.isSuccessful){
                    println("datanya ${gson.toJson(response.body())}")
                    user = response.body()!!
                    setMainContent()
                    getDataRepo()
                } else {
                    var message = "Something wrong, please try again later."
                    if (response.message().contains("limit")) {
                        message = "You reach your limit, please try again later."
                    }
                    Toast.makeText(this@DetailActivity, message, Toast.LENGTH_LONG).show()
                    finish()
                }
            }

            override fun onFailure(call: Call<Item>, t: Throwable) {
                finish()
                if (t!!.message!!.contains("resolve")) {
                    Toast.makeText(this@DetailActivity, "Offline feature still on progress", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@DetailActivity, t.message, Toast.LENGTH_SHORT).show()
                }

            }

        })
    }

    private fun setMainContent(){
        Glide.with(this).applyDefaultRequestOptions(requestOptions).load(user.avatar_url).into(iv_avatar)
        tv_login.text = user.login
        tv_name.text = user.name
        tv_bio.text = user.bio?: nullValue
        tv_followers.text = "${user.followers?:0} Followers"
        tv_following.text = "${user.following?:0} Following"
        tv_city.text = user.location?:nullValue
        tv_email.text = user.email?:nullValue
    }

    private fun getDataRepo(){
        var url = user.repos_url!!.split("api.github.com/")[1]
        val call: Call<List<ItemRepo>> = baseApi.getListRepo(url)
        call.enqueue(object : Callback<List<ItemRepo>?> {
            override fun onResponse(
                call: Call<List<ItemRepo>?>,
                response: Response<List<ItemRepo>?>
            ) {
                if(response!!.isSuccessful){
                    listRepo.clear()
                    listRepo.addAll(response.body()!!)
                    if(listRepo.size>0){
                        adapter.lists.clear()
                        adapter.lists.addAll(listRepo)
                        rv_list.visibility = View.VISIBLE
                        tv_note.visibility = View.GONE
                    } else{
                        adapter.lists.clear()
                        rv_list.visibility = View.GONE
                        tv_note.visibility = View.VISIBLE
                        tv_note.text = "User don't have repos!"
                    }
                } else {
                    tv_note.visibility = View.VISIBLE
                    var message = "Something wrong, please try again later."
                    if (response.message().contains("limit")) {
                        message = "You reach your limit, please try again later."
                    }
                    tv_note.text = message
                    rv_list.visibility = View.GONE
                    Toast.makeText(this@DetailActivity, message, Toast.LENGTH_LONG).show()
                }
                finishLoading()

            }

            override fun onFailure(call: Call<List<ItemRepo>?>, t: Throwable) {
                tv_note.visibility = View.VISIBLE
                rv_list.visibility = View.GONE
                if (t!!.message!!.contains("resolve")) {
                    tv_note.text = "Offline feature still on progress"
                } else {
                    tv_note.text = t!!.message
                }
                Toast.makeText(this@DetailActivity, t.message, Toast.LENGTH_LONG).show()
                finish()
            }
        })

    }

    override fun startLoading() {
        super.startLoading()
        tv_loading.visibility = View.VISIBLE
        ll_main.visibility = View.GONE
    }

    override fun finishLoading() {
        super.finishLoading()
        tv_loading.visibility = View.GONE
        ll_main.visibility = View.VISIBLE
    }

}