package com.example.riskaadhita.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnTouchListener
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import com.example.riskaadhita.R
import com.example.riskaadhita.adapter.UsersAdapter
import com.example.riskaadhita.api.DataResponse
import com.example.riskaadhita.base.BaseActivity
import com.example.riskaadhita.data.Item
import com.example.riskaadhita.helper.EndlessScrollListener
import com.example.riskaadhita.helper.RecyclerViewClickListener
import com.scwang.smartrefresh.header.WaterDropHeader
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : BaseActivity() {
    override fun getView(): Int = R.layout.activity_main
    lateinit var adapter: UsersAdapter
    var listUsers: ArrayList<Item> = arrayListOf()
    var page = 1

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        setRecycleView()
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query != "") {
                    getData()
                } else {
                    Toast.makeText(this@MainActivity, "Fill search box first!", Toast.LENGTH_LONG)
                        .show()
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.length > 3) {
                    getData()
                } else if (newText.isEmpty()) {
                    emptyList()
                }
                return false
            }
        })

        refresh_layout.setRefreshHeader(WaterDropHeader(this))
        refresh_layout.isEnabled = false
        refresh_layout.setOnRefreshListener(OnRefreshListener {
            page = 1
            getData()
            refresh_layout.finishRefresh()
        })
    }

    private fun getData(keterangan: String = "") {
        startLoading()
        var url = "/search/users?q=${search_view.query}&page=${page}&per_page=50"
        val call: Call<DataResponse<List<Item>>> = baseApi.getUsers(url)
        call.enqueue(object : Callback<DataResponse<List<Item>>> {
            override fun onResponse(
                call: Call<DataResponse<List<Item>>>?,
                response: Response<DataResponse<List<Item>>>?
            ) {
                finishLoading()
                if (response!!.isSuccessful) {
                    listUsers.clear()
                    listUsers.addAll(response.body()!!.items!!)
                    refresh_layout.isEnabled = true
                    if (listUsers.size > 0) {
                        if (keterangan.equals("load")) {
                            for (data in listUsers) {
                                adapter.lists.add(data)
                            }
                        } else {
                            adapter.lists.clear()
                            adapter.lists.addAll(listUsers)
                        }
                        rv_list.visibility = View.VISIBLE
                    } else {
                        adapter.lists.clear()
                        tv_note.visibility = View.VISIBLE
                        tv_note.text = "User not found."
                        rv_list.visibility = View.GONE
                        Toast.makeText(this@MainActivity, "User not found.", Toast.LENGTH_LONG)
                            .show()
                    }
                    adapter.notifyDataSetChanged()
                } else {
                    tv_note.visibility = View.VISIBLE
                    var message = "Something wrong, please try again later."
                    if (response.message().contains("limit")) {
                        message = "You reach your limit, please try again later."
                    }
                    tv_note.text = message
                    rv_list.visibility = View.GONE
                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<DataResponse<List<Item>>>?, t: Throwable?) {
                finishLoading()
                tv_note.visibility = View.VISIBLE
                rv_list.visibility = View.GONE
                if (t!!.message!!.contains("resolve")) {
                    tv_note.text = "Offline feature still on progress"
                } else {
                    tv_note.text = t!!.message
                }
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
            }
        })
    }

    fun setRecycleView() {
        adapter = UsersAdapter(this)
        rv_list.adapter = adapter
        initiateLinearLayoutRecyclerView(rv_list, object : RecyclerViewClickListener {
            override fun onClick(view: View, position: Int) {
                val myIntent = Intent(this@MainActivity, DetailActivity::class.java)
                myIntent.putExtra(
                    "data",
                    gson.toJson(adapter.lists[position])
                ) //Optional parameters
                this@MainActivity.startActivity(myIntent)
            }

            override fun onLongClick(view: View, position: Int) {
            }

        })
        rv_list.setOnTouchListener(OnTouchListener { v, event ->
            closeKeyboard()
            return@OnTouchListener false
        })
        rv_list.addOnScrollListener(
            object : EndlessScrollListener() {
                override fun onLoadMore() {
                    page += 1
                    if (listUsers.size > 19) {
                        getData("load")
                    }
                }
            }
        )
    }

    private fun closeKeyboard() {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(cv_search.windowToken, 0)
    }

    private fun emptyList() {
        tv_loading.visibility = View.GONE
        rv_list.visibility = View.GONE
        tv_note.visibility = View.VISIBLE
        tv_note.text = "Type something on search bar"
    }

    override fun startLoading() {
        super.startLoading()
        closeKeyboard()
        tv_loading.visibility = View.VISIBLE
        tv_note.visibility = View.GONE
    }

    override fun finishLoading() {
        super.finishLoading()
        tv_loading.visibility = View.GONE
    }
}