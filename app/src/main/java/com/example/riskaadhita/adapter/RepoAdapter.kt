package com.example.riskaadhita.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.riskaadhita.R
import com.example.riskaadhita.data.ItemRepo
import com.example.riskaadhita.helper.DateUtil
import kotlinx.android.synthetic.main.item_repo.view.*

class RepoAdapter(private val mContext: Context) : RecyclerView.Adapter<RepoAdapter.ViewHolder>(){
    var lists: ArrayList<ItemRepo> = arrayListOf()
    var requestOptions = RequestOptions().placeholder(R.drawable.loading_github).error(R.drawable.img_not_found).diskCacheStrategy(
        DiskCacheStrategy.AUTOMATIC).skipMemoryCache(true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false))
    override fun getItemCount(): Int = lists.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val value = lists[position]
        holder.view.tv_desc.text = value.description?: "-"
        holder.view.tv_star.text = value.stargazers_count?:"0"
        holder.view.tv_name.text = value.name
        holder.view.tv_updated.text = DateUtil().getLastUpdated(value.updated_at!!)
        Glide.with(mContext).applyDefaultRequestOptions(requestOptions).load(value.owner.avatar_url).into(holder.view.iv_avatar)
    }
    class ViewHolder(val view: View): RecyclerView.ViewHolder(view){
        var data: ItemRepo? = null
            set(value){field = value}
    }
}