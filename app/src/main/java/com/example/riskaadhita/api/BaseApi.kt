package com.healthmate.api

import com.example.riskaadhita.api.DataResponse
import com.example.riskaadhita.data.Item
import com.example.riskaadhita.data.ItemRepo
import retrofit2.Call
import retrofit2.http.*

interface BaseApi {
    @GET
    fun getUsers(@Url url: String): Call<DataResponse<List<Item>>>

    @GET
    fun getDetailUser(@Url url: String): Call<Item>

    @GET
    fun getListRepo(@Url url: String): Call<List<ItemRepo>>

}