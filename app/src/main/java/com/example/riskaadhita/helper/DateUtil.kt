package com.example.riskaadhita.helper

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    fun getLastUpdated(start_date: String): String {
        var date: String = "";
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var end_date = Date()
        try {
            val d1: Date = sdf.parse(start_date)
            val difference_In_Time: Long = end_date.time - d1.time
            val difference_In_Minutes = ((difference_In_Time
                    / (1000 * 60))
                    % 60)
            val difference_In_Hours = ((difference_In_Time
                    / (1000 * 60 * 60))
                    % 24)
            val difference_In_Days = ((difference_In_Time
                    / (1000 * 60 * 60 * 24))
                    % 365)
            if (difference_In_Days >= 7) {
                date = "Updated more than 1 week ago"
            } else if (difference_In_Days > 0) {
                date = "Updated $difference_In_Days days ago"
            } else if (difference_In_Hours > 0) {
                date = "Updated $difference_In_Hours hours ago"
            } else if (difference_In_Minutes > 1) {
                date = "Updated $difference_In_Minutes minutes ago"
            } else {
                date = "Updated just now"
            }
            println("data date $date")
        } catch (e: ParseException) {
            e.printStackTrace()
            return "";
        }
        return date;
    }
}