package com.example.riskaadhita.data

data class Item(
    var login: String = "",
    var id: String = "",
    var avatar_url: String = "",
    var url: String = "",
    var repos_url: String = "",
    var name: String = "-",
    var location: String ="-",
    var bio: String = "-",
    var email: String = "-",
    var followers: String = "0",
    var following: String = "0"
)