package com.example.riskaadhita.data

data class ItemRepo(
    var owner: Item = Item(),
    var name: String = "",
    var description: String = "",
    var updated_at: String = "",
    var stargazers_count: String = ""
)